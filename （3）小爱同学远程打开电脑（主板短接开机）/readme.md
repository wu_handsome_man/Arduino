# 小爱同学远程打开电脑（主板短接开机）

#### 实现原理
 **硬件：** WIFI模块（esp01s）、esp01s继电器、杜邦线若干、支持外接开机键的电脑、一拖二开机线  
 **软件：** Arduino、点灯科技平台、米家APP  
 **线路连接思路：**   
思路一（主板支持关机USB带电）：  
（1）连接线路时请先断电。  
（2）继电器的5v输入随意接到主板USB插座的vcc接口上，继电器的gnd接口连接USB的gnd接口。下面的图片是电脑主板USB插座的图解。  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0415/210941_3ae60722_7404083.png "屏幕截图.png")  
（3）esp01s芯片插到esp01s继电器上以后，直接由继电器供电。  
（4）一拖二开机线母头连接电脑主板上的pw+和pw-针脚，不用区分正负极。  
（5）一拖二开机线一头连接机箱的开机键，保证我们能正常使用机箱上开机键，另一头连接esp01s继电器的常闭接口（no）和公共接口（com）。这一步同样不需要区分正负极，继电器模拟的是人手按开机键的操作。开机键开机的原理是pw+和pw-针脚短接形成一个不超过3秒的电流，让主板启动，不管接口如何连接，电流都是从pw+流向排为-，不会因为杜邦线接反了而导致无法开机。  

思路二（主板不支持关机USB带电）：
（1）步骤与思路一大体相同，唯一不同的地方是第二步继电器如何在主板中取到一个5v的电。由于主板不支持USB关机供电，我们就要从其他地方找一个5v的电压，我们可以直接从主板主电源线的5VSB针脚中取到。下面是20pin和24pin针脚的定义。  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0415/214505_2a5b86fe_7404083.png "20pin.png")  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0415/214558_0302d130_7404083.png "24pin.png")  
注意：这个方法太过暴力，需要把主板电源线剪断加上一根线连接继电器，有一定的风险，别轻易尝试！！！  

 **程序配置思路：**   
（1）配置开发环境，安装arduino（可以参考我这篇[博客](https://blog.csdn.net/qq_41188880/article/details/109091037)）。  
（2）安装点灯库，并申请一个设备key（可以参考我开发小爱同学温湿度传感器的[博客](https://blog.csdn.net/qq_41188880/article/details/109092078)）  
（3）修改ino文件，修改成你的WIFI账户和密码，并添加key  
（4）烧录程序  
（5）在米家APP中绑定点灯平台，并为设备设置别名。  
（6）跟小爱同学说：“小爱同学，打开电脑”  
（7）跟小爱同学说：“小爱同学，关闭电脑”  
（8）愉快的使用吧
